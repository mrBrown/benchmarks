package org.javaforum.mrbrown.benchmark;

public class Tick {

    public final int epochSeconds;

    public final int open;

    public final int high;

    public final int low;

    public final int close;

    public Tick(final int epochSeconds, final int open, final int high, final int low, final int close) {
        this.epochSeconds = epochSeconds;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
    }

}
