package org.javaforum.mrbrown.benchmark;

import java.util.concurrent.*;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;


@Fork(1)
@BenchmarkMode(Mode.AverageTime)
@Measurement(iterations = 20, time = 1)
@State(Scope.Benchmark)
@Threads(1)
@Warmup(iterations = 20, time = 1)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class AktienBenchmark {

    Strings1 strings1;

    @Setup(Level.Invocation)
    public void setUp() {
        strings1 = new Strings1();
    }

    @Benchmark
    public void intArray(Blackhole blackhole) {
        new AktienIntArray(strings1).forEachRemaining(blackhole::consume);
    }

    @Benchmark
    public void tick(Blackhole blackhole) {
        new AktienTick(strings1).forEachRemaining(blackhole::consume);
    }
}
