package org.javaforum.mrbrown.benchmark;

import java.util.*;


class Strings1 implements Iterator<String> {

    String s;

    Scanner sca;

    Strings1() {
        Random r = new Random();
        StringJoiner j = new StringJoiner("\n");
        for (int i = 0; i <= 1111; i++) {
            j.add(i + ","
                  + (int) (r.nextFloat() * 100000) / 100000f + ","
                  + (int) (r.nextFloat() * 100000) / 100000f + ","
                  + (int) (r.nextFloat() * 100000) / 100000f + ","
                  + (int) (r.nextFloat() * 100000) / 100000f);
        }
        s = j.toString();
        sca = new Scanner(s);
    }

    @Override
    public boolean hasNext() {
        return sca.hasNextLine();
    }

    @Override
    public String next() {
        return sca.nextLine();
    }

}
