package org.javaforum.mrbrown.benchmark;

import java.util.*;


public class AktienIntArray implements Iterator<int[]> {

    Strings1 s1;

    String[] s = new String[1000];

    int i = 0;

    int j = 0;

    public AktienIntArray(final Strings1 s1) {
        this.s1 = s1;
    }

    @Override
    public boolean hasNext() {
        return i < j || s1.hasNext();
    }

    @Override
    public int[] next() {
        if (i == j) {
            i = 0;
            while (i < s.length && s1.hasNext()) {
                s[i++] = s1.next();
            }
            j = i;
            i = 0;
        }
        String[] split = s[i++].split(",");
        int[] l = new int[] {
                Integer.parseInt(split[0].split(":")[0]),
                (int) (Float.parseFloat(split[1]) * 100000),
                (int) (Float.parseFloat(split[2]) * 100000),
                (int) (Float.parseFloat(split[3]) * 100000),
                (int) (Float.parseFloat(split[4]) * 100000)};
        return l;
    }

}
